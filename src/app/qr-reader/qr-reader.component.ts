import {AfterViewInit, Component, OnDestroy, ViewChild} from '@angular/core';
import {QRCode} from 'jsqr';
import jsQR from 'jsqr';
import {fromPromise} from 'rxjs/internal-compatibility';
import {count, filter, map, switchMap, takeUntil} from 'rxjs/operators';
import {from, interval, Observable, Subject} from 'rxjs';
import {QrService} from '../qr.service';
import {Router} from '@angular/router';
import {style, trigger, state, transition, animate} from '@angular/animations';

@Component({
  selector: 'app-qr-reader',
  templateUrl: './qr-reader.component.html',
  styleUrls: ['./qr-reader.component.css'],
  animations: [
    trigger('Add', [
      state('add', style({
        backgroundColor: '#ffffff'
      })),
      state('added', style({
        backgroundColor: '#247cff'
      })),
      transition('add => added', [
        animate('1s')
      ]),
      transition('added => add', [
        animate('0s')
        ])
    ])
  ]

})
export class QrReaderComponent implements AfterViewInit, OnDestroy {

  capture$: Observable<QRCode>;
  closeSignal: Subject<any>;

  capturable = true;
  code: any;
  @ViewChild('video') video;
  videoElm: HTMLVideoElement;
  @ViewChild('canvas') canvas;
  canvasElm: HTMLCanvasElement;

  ctx: CanvasRenderingContext2D;

  count: Observable<number>;

  error: Error;

  readonly medias: MediaStreamConstraints = {
    audio: false,
    video: true
  };

  constructor(private qrService: QrService, private router: Router) {
    this.count = qrService.count();
    this.closeSignal = new Subject();
  }

  ngAfterViewInit() {
    this.cameraSetup();
  }

  onVideoLoad() {
    this.capturable = true;
  }

  captureShot() {

    const width = this.videoElm.clientWidth;
    const height = this.videoElm.clientHeight;

    this.canvasElm.width = width;
    this.canvasElm.height = height;

    this.ctx.drawImage(this.videoElm, 0, 0);
    const data = this.ctx.getImageData(0, 0, width, height).data;
    return jsQR(data, width, height);
  }

  cameraSetup() {

    this.videoElm = this.video.nativeElement;
    this.canvasElm = this.canvas.nativeElement;
    this.ctx = this.canvasElm.getContext('2d');


    this.capture$ = fromPromise(navigator.mediaDevices.enumerateDevices())
      .pipe(
        switchMap(infoList => from(infoList)),
        count(info => info.kind === 'videoinput'),
        switchMap(deviceCount => {
          this.medias.video = {facingMode: deviceCount >= 2 ? {exact: 'environment'} : 'user'};
          return fromPromise(navigator.mediaDevices.getUserMedia(this.medias));
        }),
        switchMap(stream => {
          this.videoElm.srcObject = stream;
          return interval(100);
        }),
        map(() => this.captureShot()),
        filter(code => code !== null),
        takeUntil(this.closeSignal)
      );

    this.capture$
      .subscribe(
        code => this.qrService.add(code),
        error => alert(error),
        () => {
          const stream = this.videoElm.srcObject as MediaStream;
          stream.getTracks().forEach(track => track.stop());
        }
      );
  }

  flash() {

  }

  close() {
    this.router.navigate(['/qr']);
  }

  ngOnDestroy(): void {
    this.closeSignal.next();
  }
}
