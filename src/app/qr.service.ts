import { Injectable } from '@angular/core';
import { QRCode } from 'jsqr';
import { BehaviorSubject, Observable, of, Subject } from 'rxjs';
import { count, switchMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class QrService {

  private _list: Map<string, QRCode> = new Map();
  private _list$: Subject<QRCode[]>;

  constructor() {
    this._list$ = new BehaviorSubject(this.toArray());
  }

  list(): Observable<QRCode[]> {
    return this._list$.asObservable();
  }

  add(code: QRCode) {
    this._list.set(code.data, code);
    this._list$.next(this.toArray());
  }

  count(): Observable<number> {
    return this._list$
      .pipe(
        switchMap(list => of(list.length))
      );
  }

  private toArray(): Array<QRCode> {
    return Array.from(this._list.values());
  }
}
