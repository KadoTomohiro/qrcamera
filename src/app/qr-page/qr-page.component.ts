import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { QRCode } from 'jsqr';
import { QrService } from '../qr.service';

@Component({
  selector: 'app-qr-page',
  templateUrl: './qr-page.component.html',
  styleUrls: ['./qr-page.component.css']
})
export class QrPageComponent implements OnInit {

  list: Observable<QRCode[]>;
  count: Observable<number>;
  constructor(private qrService: QrService) { }

  ngOnInit() {
    this.list = this.qrService.list();
    this.count = this.qrService.count();
  }

}
