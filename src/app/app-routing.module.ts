import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { QrPageComponent } from './qr-page/qr-page.component';
import { QrReaderComponent } from './qr-reader/qr-reader.component';
import { BarcodePageComponent } from './barcode-page/barcode-page.component';

const routes: Routes = [
  {path: 'qr', component: QrPageComponent},
  {path: 'qr/camera', component: QrReaderComponent},
  {path: 'barcode', component: BarcodePageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
