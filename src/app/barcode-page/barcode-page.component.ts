import { Component, OnInit, ViewChild } from '@angular/core';
import { fromEvent, timer } from 'rxjs';
import { buffer, debounceTime, filter, map, tap } from 'rxjs/operators';

@Component({
  selector: 'app-barcode-page',
  templateUrl: './barcode-page.component.html',
  styleUrls: ['./barcode-page.component.css']
})
export class BarcodePageComponent implements OnInit {

  list: string[] = [];
  constructor() {
  }

  ngOnInit() {
    const ENTER_CODE = 'Enter';
    const keypress = fromEvent(document, 'keypress');
    keypress
      .pipe(
        filter((e: KeyboardEvent) => e.key !== null || e.key === ENTER_CODE),
        map((e: KeyboardEvent) => e.key),
        buffer(keypress.pipe(debounceTime(100))),
        filter((chars: string[]) => chars[chars.length - 1] === ENTER_CODE),
        map((chars: string[]) => chars.slice(0, -1).join(''))
      )
      .subscribe(barcode => {
        this.list.push(barcode);
      });
  }



}
